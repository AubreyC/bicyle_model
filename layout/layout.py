# -*- coding: utf-8 -*-
# @Author: Aubrey
# @Date:   2019-05-16 16:37:22
# @Last Modified by:   Aubrey
# @Last Modified time: 2019-05-28 16:18:12

import numpy as np

class AbstractLayout(object):
    """Abstrct Layout: Grid shape structure used to store information. Channel are used to store specific informations."""

    def __init__(self, width, channel, scale = 0.2, origin_world = (0,0)):

        self.scale = scale;
        self._width = width;
        self._channel = channel;

        self._array = np.zeros([width, width, channel]);

        """ Define the position of the origin of the world coordinate in the layout """
        self._origin_world = origin_world;

    def reset(self):
        """Reset the layout with 0 everywhere
        """

        self._array = np.zeros([width, width, channel]);


    def check_in_array(self, x, y, channel):
        """Check if the point defined by x, y, channel is inside the layout

        Args:
            x (TYPE): position x in layout
            y (TYPE): position y in layout
            channel (TYPE): channelin layout

        Returns:
            TYPE: Boolean to indicates if point is inside the layout
        """
        result = (x > 0 and x < self._array.shape[0]) and \
                 (y > 0 and y < self._array.shape[1]) and \
                 (channel > 0 and channel < self._array.shape[2]);

        return result;

    def set_value(self, x, y, channel, value):
        """Set the value in the layout

        Args:
            x (TYPE): x layout position
            y (TYPE): y layout position
            channel (TYPE): channel layout position
            value (TYPE): Value

        Returns:
            TYPE: success flag
        """

        success = False;
        if self.check_in_array(x, y, channel):
            self._array[x,y,channel] = value;
            success = True;

        return success;

    def get_value(self, x, y, channel):
        """Get value at specific position in layout

        Args:
            x (TYPE): x layout position
            y (TYPE): y layout position
            channel (TYPE): channel layout position

        Returns:
            TYPE: Value
        """
        value = None;
        if self.check_in_array(x, y, channel):
            value = self._array[x,y,channel];

        return value;

    def convert_world_to_layout(self, pos_world):
        """ Convert position in world frame into layout coordinate frame

        Args:
            pos_world (TYPE): Position in world frame

        Returns:
            TYPE: pos_layout Position in layout frame

        Raises:
            ValueError: Description
        """

        if len(pos_world) != 2:
            raise ValueError('[AbstractLayout]: pos_world wrong size {}'.format(len(pos_world)))

        # Convert into layout position
        pos_layout = (int(pos_world[0] // self.scale + self._origin_world[0]), int(pos_world[1] // self.scale + self._origin_world[1]));
        return pos_layout

    def convert_layout_to_world(self, pos_layout):
        """Convert position in layout frame into world coordinate frame

        Args:
            pos_layout (TYPE): Position in layout frame

        Returns:
            TYPE: pos_world Position in world frame

        Raises:
            ValueError: Raise error if wrong size for the input
        """
        if len(pos_layout) != 2:
            raise ValueError('[AbstractLayout]: pos_layout wrong size {}'.format(len(pos_layout)))

        # Convert into layout position
        pos_world = ((pos_layout[0] - self._origin_world[0]) * self.scale, (pos_layout[1] - self._origin_world[1]) * self.scale);
        return pos_world;

class VehicleLayout(AbstractLayout):
    """Layout for vehicles: In a tensor shape with channel:
    - car_id,
    - vx
    - vy
    - psi_rad
    """

    VEHICLELAYOUT_CHANNEL = 4;

    ID_CHANNEL = 0;
    VX_CHANNEL = 1;
    VY_CHANNEL = 2;
    PSI_CHANNEL = 3;

    def __init__(self, width, scale = 0.2):
        super(VehicleLayout, self).__init__(width, self.VEHICLELAYOUT_CHANNEL, scale);

    def add_vehicles(self, vehicles):

        for v in vehicles:

            # Get world position
            pos_world = (v.state_x, v.state_y);

            # Convert in layout position
            pos_layout = self.convert_world_to_layout(pos_world);

            # Set values:
            self.set_value(pos_layout[0], pos_layout[1], self.ID_CHANNEL, v.vehicle_id);
            self.set_value(pos_layout[0], pos_layout[1], self.VX_CHANNEL, v.state_vx);
            self.set_value(pos_layout[0], pos_layout[1], self.VY_CHANNEL, v.state_vy);
            self.set_value(pos_layout[0], pos_layout[1], self.PSI_CHANNEL, v.state_psi);

class RoadLayout(object):
    """Layout for vehicles: In a tensor shape with channel:
    - drivable
    """

    ROADLAYOUT_CHANNEL = 1;

    DRIVABLE_VALUE = 1;
    NONDRIVABLE_VALUE = 0;

    def __init__(self, width, scale = 0.2):
        super(RoadLayout, self).__init__(width, self.ROADLAYOUT_CHANNEL, scale);