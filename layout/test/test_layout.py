# -*- coding: utf-8 -*-
# @Author: caor
# @Date:   2019-03-14 15:02:43
# @Last Modified by:   Aubrey
# @Email: clausse.aubrey@gmail.com
# @Github:

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import math

from vehicle.vehicle import Vehicle
from layout.layout import VehicleLayout;

# Dummy result of the test, only check if it runs.
def test_vehicle_layout():

    width_layout = 100;

    vehicle_layout = VehicleLayout(width_layout);

    # Create Bicycle model car_model
    vehicles = [];
    for i in range(10):

        # Initial condition
        pos_init = np.random.uniform(0, 10, 2);
        v_init = np.random.uniform(0, 10);
        psi_init = np.random.uniform(0, np.pi);

        # Create vehicle
        v = Vehicle(i, pos_init[0], pos_init[1], v_init, psi_init);

        # Add vehicle to the list
        vehicles.append(v);

    # Add vehicle to the layout
    vehicle_layout.add_vehicles(vehicles);

if __name__ == '__main__':

    try:
        test_vehicle_layout()
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')