from setuptools import setup

setup(name="bicycle_model",
      install_requires=['cycler',
                        'matplotlib',
                        'numpy',
                        'six',
                        'pygame',
                        'python-dateutil'
                        ])
