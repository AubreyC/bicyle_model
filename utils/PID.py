# -*- coding: utf-8 -*-
# @Author: Aubrey
# @Date:   2019-05-14 16:23:50
# @Last Modified by:   Aubrey
# @Last Modified time: 2019-06-03 16:13:27



class PID(object):
    """docstring for PID"""
    def __init__(self, delta_s, kP=0.0, kI=0.0, kD=0.0, ):

        self.delta_s = delta_s;

        self.kP = kP;
        self.kI = kI;
        self.kD = kD;

        self.integral_e = 0.0;

        self.prev_e = None;

    def reset():

        self.integral = 0.0;

    def get_control(self, target_value, current_value):

        # Compute error
        e = float(target_value - current_value);

        # COmpute intergal
        self.integral_e += e*self.delta_s;

        #Compute error derivative
        e_dot = 0.0;
        if not (self.prev_e is None):
            e_dot = float(e - self.prev_e)/float(self.delta_s);

        # Save last error for next error derivative
        self.prev_e = e;

        return self.kP*e + self.kI*self.integral_e + self.kD*e_dot;
