# -*- coding: utf-8 -*-
# @Author: Aubrey
# @Date:   2019-05-15 16:20:30
# @Last Modified by:   Aubrey
# @Last Modified time: 2019-05-15 17:29:42

import pygame

BLACK = (0, 0, 0);
WHITE = (255, 255, 255);
RED = (255, 0, 0);
GREEN = (0, 255, 0);
BLUE = (0, 0, 255);

class Graphics(object):
    """docstring for Graphics"""
    def __init__(self):

        pygame.init()
        pygame.display.set_caption("Car");

        self.screen = pygame.display.set_mode((720, 480))
        self.clock = pygame.time.Clock()
        self.FPS = 60  # Frames per second.

    def display(self):

            self.clock.tick(self.FPS)

            # for event in pygame.event.get():
            #     if event.type == pygame.QUIT:
            #         quit()
            #     elif event.type == pygame.KEYDOWN:
            #         if event.key == pygame.K_w:
            #             rect.move_ip(0, -2)
            #         elif event.key == pygame.K_s:
            #             rect.move_ip(0, 2)
            #         elif event.key == pygame.K_a:
            #             rect.move_ip(-2, 0)
            #         elif event.key == pygame.K_d:
            #             rect.move_ip(2, 0)



            self.screen.fill(BLACK)
            self.screen.blit(image, rect)

            pygame.display.update()  # Or pygame.display.flip()





if __name__ == '__main__':
    graph = Graphics();