# -*- coding: utf-8 -*-
# @Author: caor
# @Date:   2019-03-14 15:02:43
# @Last Modified by:   Aubrey
# @Email: clausse.aubrey@gmail.com
# @Github:

##########################################################################################
#
# Simple Car Bicycle model:
# The Kinematic Bicycle Model: a Consistent Model forPlanning Feasible Trajectories for Autonomous Vehicles? - Philip Polack
# https://hal-polytechnique.archives-ouvertes.fr/hal-01520869/document
#
##########################################################################################

import numpy as np
import math
import copy

class ParamStruct():
    def __init__(self, lr = 1.0, lf = 1.0, w=1.0):
        self.lr = float(lr); # Distance between CG to rear wheel
        self.lf = float(lf); # Distance between CG to front wheel
        self.w = float(w);   # Width of the car (only used for plotting)

class BMLogStruct(object):
    """docstring for LogStruct"""
    def __init__(self, state, control, step):
        self.state = copy.copy(state);
        self.control = copy.copy(control);
        self.step = copy.copy(step);

class BicycleModel(object):
    """docstring for BycicleModel"""
    def __init__(self, step_init = 0, dt = 0.1, x = 0., y= 0., v=0., psi=0.):

        # State: [x,y,v,psi, beta]: At center of gravity
        self._state = np.array([x, y, v, psi, 0.], dtype=float)

        self._control = np.array([0., 0.]);
        self._step = step_init;

        self._log = [];

        self._params = ParamStruct();
        self._dt = float(dt);

        self._save_state();

    def step(self, control):

        # Get control inputs
        if control.shape != (2,):
            raise ValueError('[ERROR]: Shape of the control input not correct: {}'.format(control.shape))

        #Save control
        self._control = control;

        u_accel = control[0];
        u_steer = control[1];

        # Get state variables
        x = self._state[0];
        y = self._state[1];
        v = self._state[2];
        psi = self._state[3];

        # Compute derivative
        v_dot = u_accel;

        beta = np.arctan(np.tan(u_steer)*(self._params.lr/(self._params.lf + self._params.lr)));
        # print(beta)

        psi_dot = (v/self._params.lr)*np.sin(beta);

        x_dot = v*np.cos(psi + beta);
        y_dot = v*np.sin(psi + beta);

        # Crude intergation:
        x_new = x + x_dot*self._dt;
        y_new = y + y_dot*self._dt;
        v_new = v + v_dot*self._dt;
        psi_new = psi + psi_dot*self._dt;

        self._state[0] = x_new;
        self._state[1] = y_new;
        self._state[2] = v_new;
        self._state[3] = psi_new;
        self._state[4] = beta;

        # Update Tick:
        self._step +=1;

        # Save new state in the history
        self._save_state();

    def _save_state(self):

        # print("Save state: {}".format(self._step))
        log_data = BMLogStruct(self._state, self._control, self._step);
        self._log.append(log_data);

    @property
    def state_x_rear(self):
        return self._state[0] - self.param_lr*np.cos(self.state_psi);

    @property
    def state_y_rear(self):
        return self._state[1] - self.param_lr*np.sin(self.state_psi);

    @property
    def state_x(self):
        return self._state[0];

    @property
    def state_y(self):
        return self._state[1];

    @property
    def state_v(self):
        return self._state[2];

    @property
    def state_vx(self):
        x_dot = self.state_v*np.cos( self.state_psi +  self.state_beta);
        return x_dot;

    @property
    def state_vy(self):
        y_dot = self.state_v*np.sin( self.state_psi +  self.state_beta);
        return y_dot;

    @property
    def state_psi(self):
        return self._state[3];

    @property
    def state_beta(self):
        return self._state[4];

    @property
    def param_lf(self):
        return self._params.lf;

    @property
    def param_lr(self):
        return self._params.lr;

    @property
    def param_w(self):
        return self._params.w;