# -*- coding: utf-8 -*-
# @Author: caor
# @Date:   2019-03-14 15:02:43
# @Last Modified by:   Aubrey
# @Email: clausse.aubrey@gmail.com
# @Github:

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import math

from vehicle import bicycle_dynamics

def rotate_by_theta(point, theta=math.pi):
    M = np.array([[np.cos(theta), -1 * np.sin(theta)],
                     [np.sin(theta), np.cos(theta)]])
    return np.matmul(M, point.transpose()).transpose()

def run_test_bicycle_model(display=False):

    # Create Bicycle model car
    bm_car = bicycle_dynamics.BicycleModel();

    # Run the model
    for i in range(10000):

        # Create control input: Constant steering angle and changeing acceleration
        u_accel = 0.0;
        u_steer = 0.2;
        if i%1000 < 500:
            u_accel = 0.05
        if i%1000 > 500:
            u_accel = -0.05;
        control = np.array([u_accel, u_steer]);

        # Run the model
        bm_car.step(control);

        print('BM: x: {} y:{} v:{} psi:{}'.format(bm_car.state_x, bm_car.state_y, bm_car.state_v, bm_car.state_psi))

    if display:
        # Plot results
        plt.figure()
        plt.xlim(-30,30)
        plt.ylim(-30,30)
        plt.ylabel('Position y (m)')
        plt.ylabel('Position x (m)')
        plt.title('Position X Y')

        # Define rectangle to plot the car
        rect_car_og = np.array([[-bm_car.param_lr,-bm_car.param_w/2],[-bm_car.param_lr,bm_car.param_w/2],[bm_car.param_lf,bm_car.param_w/2],[bm_car.param_lf,-bm_car.param_w/2]]);

        # Get Vehicle State
        x = bm_car._log[0].state[0];
        y = bm_car._log[0].state[1];
        psi = bm_car._log[0].state[3];

        # Create rectangle: Rotate only the rectangle and then move it to the actual car position
        rect_car = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_og, psi);

        # Add the car to the plot
        rect_poly = plt.Polygon(rect_car, fc='r')
        plt.gca().add_patch(rect_poly)

        for i in range(10000):

            # Do not plot at each timestep
            if i%10 == 0:

                # Get Vehicle State
                x = bm_car._log[i].state[0];
                y = bm_car._log[i].state[1];
                psi = bm_car._log[i].state[3];

                print('BM Display: step:{}'.format(bm_car._log[i].step))

                print('BM Display: x: {} y:{} psi:{}'.format(x, y, psi))

                rect_car = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_og, psi);

                rect_poly.set_xy(rect_car);

                plt.pause(0.00001) # pause with time in secodns

        plt.show()

    return True;

# Dummy result of the test, only check if it runs.
def test_bicycle_model():
    assert run_test_bicycle_model();

if __name__ == '__main__':

    try:
        run_test_bicycle_model(True)
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')