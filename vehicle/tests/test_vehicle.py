# -*- coding: utf-8 -*-
# @Author: caor
# @Date:   2019-03-14 15:02:43
# @Last Modified by:   Aubrey
# @Email: clausse.aubrey@gmail.com
# @Github:

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import math

from vehicle.vehicle import Vehicle
from vehicle.vehicle import WaypointsVehicle
from utils.waypoints import WaypointsUtils

def rotate_by_theta(point, theta=math.pi):
    M = np.array([[np.cos(theta), -1 * np.sin(theta)],
                     [np.sin(theta), np.cos(theta)]])
    return np.matmul(M, point.transpose()).transpose()

def run_test_vehicle(display = False):

    # Create Bicycle model car_model
    car_model = Vehicle(0, dt=0.1);

    if display:
        # Plot results
        plt.figure()
        plt.xlim(-30,30)
        plt.ylim(-30,30)
        plt.ylabel('Position y (m)')
        plt.ylabel('Position x (m)')
        plt.title('Position X Y')

        # Define rectangle to plot the car_model
        rect_car_model_og = np.array([[-car_model.param_lr,-car_model.param_w/2],[-car_model.param_lr,car_model.param_w/2],[car_model.param_lf,car_model.param_w/2],[car_model.param_lf,-car_model.param_w/2]]);

        # Get Vehicle State
        x = car_model._log[0].state[0];
        y = car_model._log[0].state[1];
        psi = car_model._log[0].state[3];

        # Create rectangle: Rotate only the rectangle and then move it to the actual car_model position
        rect_car_model = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_model_og, psi);

        # Add the car_model to the plot
        rect_poly = plt.Polygon(rect_car_model, fc='b')
        plt.gca().add_patch(rect_poly)

    # Run the model
    for i in range(10000):

        # Create control input: Constant steering angle and changeing acceleration
        u_accel = 0.0;
        u_steer = 0.2;
        if i%1000 < 500:
            u_accel = 0.05
        if i%1000 > 500:
            u_accel = -0.05;
        control = np.array([u_accel, u_steer]);

        # Run the model
        car_model.step(control);

        print('car_model: x: {} y:{} v:{} psi:{}'.format(car_model.state_x, car_model.state_y, car_model.state_v, car_model.state_psi))

        if display:

            # Do not plot at each timestep
            if i%10 == 0:

                # Get Vehicle State
                x = car_model._log[i].state[0];
                y = car_model._log[i].state[1];
                psi = car_model._log[i].state[3];

                rect_car_model = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_model_og, psi);

                rect_poly.set_xy(rect_car_model);

                plt.pause(0.00001) # pause with time in secodns

    plt.show()

    return True;

def run_test_waypoints(display = False):

    # Create Bicycle model car_model
    car_model = WaypointsVehicle(0);

    #Turn traj
    waypoints = WaypointsUtils.create_waypoints_sin();
    for wp in waypoints:
        car_model.add_waypoint(wp);

    if display:
        # Plot results
        plt.figure()
        plt.xlim(-10,100)
        plt.ylim(-50,50)
        plt.ylabel('Position y (m)')
        plt.ylabel('Position x (m)')
        plt.title('Position X Y')


        # Plot waypoints:
        wp_x = [];
        wp_y = [];
        for wp in car_model.waypoints:
            wp_x.append(wp[0]);
            wp_y.append(wp[1]);

        plt.plot(wp_x, wp_y, "-r", label="course")

        wp_circle = plt.Circle(np.array([wp_x[0], wp_y[0]]), radius=0.5, fc='g')
        plt.gca().add_patch(wp_circle)


        # Define rectangle to plot the car_model
        rect_car_model_og = np.array([[-car_model.param_lr,-car_model.param_w/2],[-car_model.param_lr,car_model.param_w/2],[car_model.param_lf,car_model.param_w/2],[car_model.param_lf,-car_model.param_w/2]]);

        # Get Vehicle State
        x = car_model._log[0].state[0];
        y = car_model._log[0].state[1];
        psi = car_model._log[0].state[3];

        # Create rectangle: Rotate only the rectangle and then move it to the actual car_model position
        rect_car_model = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_model_og, psi);

        # Add the car_model to the plot
        rect_poly = plt.Polygon(rect_car_model, fc='b')
        plt.gca().add_patch(rect_poly)


    # Run the model
    for i in range(10000):

        car_model.follow_waypoints();

        print('car_model: x: {} y:{} v:{} psi:{}'.format(car_model.state_x, car_model.state_y, car_model.state_v, car_model.state_psi))

        if car_model.finished:
            print('Waypoints finished x: {} y:{} v:{} psi:{}'.format(car_model.state_x, car_model.state_y, car_model.state_v, car_model.state_psi))
            break;

        if display:

            # Do not plot at each timestep
            if i%10 == 0:

                # Plot the current waypoint:
                wp = car_model.get_waypoint_by_index(car_model.current_waypoint_index);
                wp_circle.center = (wp[0], wp[1]);


                # Get Vehicle State
                x = car_model._log[i].state[0];
                y = car_model._log[i].state[1];
                psi = car_model._log[i].state[3];

                rect_car_model = np.array([[x,y],[x,y],[x,y],[x,y]]) + rotate_by_theta(rect_car_model_og, psi);

                rect_poly.set_xy(rect_car_model);

                plt.pause(0.1) # pause with time in secodns
                # plt.pause(0.00001) # pause with time in secodns

    # plt.show()

    return True;

# Dummy result of the test, only check if it runs.
def test_vehicle_model():
    assert run_test_vehicle();

    assert run_test_waypoints();

if __name__ == '__main__':

    try:
        # run_test_vehicle(True);
        run_test_waypoints(True)

    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')