# -*- coding: utf-8 -*-
# @Author: Aubrey
# @Date:   2019-05-15 16:20:30
# @Last Modified by:   Aubrey
# @Last Modified time: 2019-06-03 17:39:24

from utils.PID import PID
from vehicle.bicycle_dynamics import BicycleModel

import numpy as np
import math
import pygame

# def display(vehicle, surface):
#     v = vehicle
#     s = pygame.Surface((surface.pix(v.LENGTH), surface.pix(v.LENGTH)), pygame.SRCALPHA)  # per-pixel alpha
#     rect = (0, surface.pix(v.LENGTH) / 2 - surface.pix(v.WIDTH) / 2, surface.pix(v.LENGTH), surface.pix(v.WIDTH))
#     pygame.draw.rect(s, cls.get_color(v, transparent), rect, 0)
#     pygame.draw.rect(s, cls.BLACK, rect, 1)
#     s = pygame.Surface.convert_alpha(s)


class Vehicle(BicycleModel):
    """A simple vehicle that can be controller with acceleration and steering angle"""

    def __init__(self, vehicle_id, dt = 0.1, x = 0., y= 0., v=0., psi=0.):

         super(Vehicle, self).__init__(dt = dt, x=x, y=y, v=v, psi=psi);

         self.vehicle_id = vehicle_id;

class WaypointsVehicle(Vehicle):
    """Vehicle controlled with a Pure Poursuite controller to follow a lits of waypoints"""

    TARGET_VELOCIY = 5.0;         # Meters/seconds

    MAX_WP_FROM_CURRENT = 5; # Difference of index between the closest waypoint used for PP and the current wp
    LOOKAHEAD_GAIN = 0.1  # Lookahead gain used to compute lookahead as a function of the current velocity (meters/seconds)
    LOOKAHEAD_DIST = 0.1  # Lookahead distance (meters)

    VELOCITY_KP = 1.0;    # Proportional gain used for velocity controller

    END_WAYPOINT_DIST = 3.0 # If closer than this distance from the last waypoint;

    def __init__(self, vehicle_id, dt = 0.1, x = 0., y= 0., v=0., psi=0.):

        super(WaypointsVehicle, self).__init__(vehicle_id, dt, x=x, y=y, v=v, psi=psi)

        self.waypoints = [];
        self.current_waypoint_index = 0;

        self.target_velocity = self.TARGET_VELOCIY;

        self.PID_velocity = PID(dt, kP=self.VELOCITY_KP);

        self.finished = False;   # Flag used to detect if the vehicle reached the end of the waypoints

    def add_waypoint(self, waypoint):
        """Add waypoints to the list of waypoints to follow

        Args:
            waypoint (TYPE): Waypoint as np.array of shape = (2,1)

        Raises:
            Exception: Description
        """

        # Control waypoint format
        if waypoint.shape != (2,1):
            raise ValueError('[ERROR]: Shape of the waypoint input not correct: {}'.format(waypoint.shape))

        self.waypoints.append(waypoint);


    def set_target_velocity(self, target_velocity):
        """Set the target velocity used to follow the waypoints

        Args:
            target_velocity (TYPE): Target veolcity in m/s
        """
        self.target_velocity = target_velocity;


    def follow_waypoints(self):
        """Follow the waypoint trajectory using a Pure Pursuit controller:
        - https://github.com/AtsushiSakai/PythonRobotics/blob/master/PathTracking/pure_pursuit/pure_pursuit.py
        - https://www.ri.cmu.edu/pub_files/pub3/coulter_r_craig_1992_1/coulter_r_craig_1992_1.pdf

        """
        # Latitude controller
        self.current_waypoint_index, lookahead_dist = self.get_current_waypoint(self.current_waypoint_index);

        # compute alpha
        current_waypoint = self.get_waypoint_by_index(self.current_waypoint_index);

        # Compute angle between the vehicle heading and vector from vehicle rear to waypoint
        alpha = np.arctan2(current_waypoint[1] - self.state_y_rear, current_waypoint[0] - self.state_x_rear) - self.state_psi;

        # Compute steering angle
        u_steer = np.arctan2((2*(self.param_lf + self.param_lr)*np.sin(alpha))/float(lookahead_dist), 1.0);

        # Velocity controller: Simple proportional controller
        u_accel = self.PID_velocity.get_control(self.target_velocity, self.state_v);

        # Send the action to the dynamic model
        self.step(np.array([u_accel, u_steer]));

        # Check if the trajectory is done
        self.finished = self.is_finished();

        return self.finished;

    def is_finished(self):

        last_wp = self.get_waypoint_by_index(-1);

        dist = self.dist_to_waypoint(last_wp);

        finished = False;
        if dist < self.END_WAYPOINT_DIST:
            finished = True;

        return finished;

    def get_waypoint_by_index(self, waypoint_index):

        # Lat controleller
        if len(self.waypoints) == 0 or waypoint_index < -1 or waypoint_index > len(self.waypoints) -1:
            raise Exception('[ERROR]: Current waypoint out of bounds: ind:{} waypoints: {}'.format(current_waypoint_index, len(self.waypoints)))

        return self.waypoints[waypoint_index];



    def get_current_waypoint(self, current_waypoint_index):
        """Get the waypoint to look to. Inspired from:
        - https://github.com/AtsushiSakai/PythonRobotics/blob/master/PathTracking/pure_pursuit/pure_pursuit.py
        - https://www.ri.cmu.edu/pub_files/pub3/coulter_r_craig_1992_1/coulter_r_craig_1992_1.pdf

        Returns:
            TYPE: Waypoint index

        Raises:
            Exception: Description
        """

        # Get closest waypoint:
        min_dist = None;
        min_ind = None;

        # Looking at waypoints around the current waypoint only to avoid jumping to other part of the trajectory by mistake
        min_wp_ind = max(0, self.current_waypoint_index - self.MAX_WP_FROM_CURRENT);
        max_wp_ind = min(self.current_waypoint_index + self.MAX_WP_FROM_CURRENT, len(self.waypoints));
        for index in range(min_wp_ind, max_wp_ind):

            dist = self.dist_to_waypoint(self.get_waypoint_by_index(index));

            # print('get_current_waypoint: dist: {} min_ind:{}'.format(dist, index))

            if not min_dist:
                min_dist = dist;
                min_ind = index;

            if dist < min_dist:
                min_dist = dist;
                min_ind = index;

        # Compute look ahead distance:
        Lf = self.LOOKAHEAD_GAIN * self.state_v + self.LOOKAHEAD_DIST;

        # search look ahead target point index
        lookahead_index = min_ind;
        lookahead_dist = 0.0;
        while Lf > lookahead_dist and (lookahead_index + 1) < len(self.waypoints):

            lookahead_dist = self.dist_to_waypoint(self.get_waypoint_by_index(lookahead_index));
            lookahead_index += 1

        return lookahead_index, lookahead_dist;

    def dist_to_waypoint(self, waypoint):
        """Compute current distance to a waypoint

        Args:
            waypoint (TYPE): Waypoint as a np.array of shape (2,1)

        Returns:
            TYPE: Description
        """

        # Get current position of the vehicle
        position = np.array([self.state_x_rear, self.state_y_rear]);
        position.shape = (2,1);

        # Get dist to the waypoint
        dist = np.linalg.norm(position - waypoint);

        return dist;